# -*- coding: utf-8 -*-

import stomp
import time
from simplejson import loads, dumps
from send_query import post_dict
import uuid
import utils
from datetime import datetime

class EventHandler(object):

	def persist_tag_like(self,message={}):
		#import pdb;pdb.set_trace()
		context = message.get("context",{})
		data = message.get("data",{})
		ref_ao = context.get('ref_ao')
		f_ref = data.get("freelance_ref","")
		tag_name = data.get("tag_name","")
		tag_state = data.get("tag_state","")

		if ref_ao and f_ref and tag_name and tag_state:
			action = ""
			if tag_state == 1:
				action = "LIKE"
			elif tag_state == 2:
				action = "DISLIKE"
			else:
				return ("ERROR")
			query = """"""


	def persist_search_freelance(self,message={}):
		node_type = "SEARCH_FREELANCE_EVENT"
		now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
		event_properties = {
								"ref" : str(uuid.uuid4()),
								"external_ref" : "",
								"name" : "search_freelance",
								"created_date" : now,
								"archived_date" : "",
								"is_archived" : "false",
								"status" : ""
		                 }
		data = utils.flatten_json({"data":message.get("data",{})})
		event_properties.update(data)

		query = ""
		end_query = """CREATE (event:%(node_type)s) """%locals()
		for key,val in event_properties.items():
			end_query += """SET event.%(key)s="%(val)s" """%locals()

		count = 0
		for node in message.get("context",[]):
			node_type = node.get("role","")
			node_ref = node.get("ref","")
			if node_type and node_ref:
				count += 1
				query += """MATCH (n%(count)s:%(node_type)s{ref:"%(node_ref)s"}) """%locals()
				end_query += """MERGE (n%(count)s)<-[r:HAS_CONTEXT]-(event) SET r.created_date="%(now)s" """%locals()

		
		query += end_query
		status,data = post_dict(dictionary={"query": query})
		report = "event created" if status=="OK" else "Bad comm with DB"
		return (status,report)


	def persist_freelance_apply_ao(self,message={}):
		node_type = "FREELANCE_APPLY_AO_EVENT"
		now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
		event_properties = {
								"ref" : str(uuid.uuid4()),
								"external_ref" : "",
								"name" : "freelance_apply_ao",
								"created_date" : now,
								"archived_date" : "",
								"is_archived" : "false",
								"status" : ""
		                 }

		data = utils.flatten_json({"data":message.get("data",{})})
		event_properties.update(data)

		query = ""
		end_query = """CREATE (event:%(node_type)s) """%locals()
		for key,val in event_properties.items():
			end_query += """SET event.%(key)s="%(val)s" """%locals()

		count = 0
		for node in message.get("context",[]):
			node_type = node.get("role","")
			node_ref = node.get("ref","")
			if node_type and node_ref:
				count += 1
				query += """MATCH (n%(count)s:%(node_type)s{ref:"%(node_ref)s"}) """%locals()
				end_query += """MERGE (n%(count)s)<-[r%(count)s:HAS_CONTEXT]-(event) SET r%(count)s.created_date="%(now)s" """%locals()

		
		query += end_query
		status,data = post_dict(dictionary={"query": query})
		report = "event created" if status=="OK" else "Bad comm with DB"
		return (status,report)


class SearchFreelanceListener(stomp.ConnectionListener):

	def __init__(self,*args,**kwargs):
		super(SearchFreelanceListener, self).__init__()
		self.event_handler = EventHandler()

	def on_error(self, headers, message):
		print('************************** received an error "%s" **************************' % message)

	def on_message(self, headers, message):
		print('************************** received a message "%s **************************"' % message)
		message = loads(message,encoding='utf8')
		return self.event_handler.persist_search_freelance(message)


class FreelanceApplyAoListener(stomp.ConnectionListener):

	def __init__(self,*args,**kwargs):
		super(FreelanceApplyAoListener, self).__init__()
		self.event_handler = EventHandler()

	def on_error(self, headers, message):
		print('************************** received an error "%s" **************************' % message)

	def on_message(self, headers, message):
		print('************************** received a message "%s **************************"' % message)
		message = loads(message,encoding='utf8')
		return self.event_handler.persist_freelance_apply_ao(message)


class StompConn(object):
	def __init__(self,host='',port='',event=''):
		self.conn = stomp.Connection([('dev.freelance.com',61613)])
		self.conn.start()
		self.conn.connect('admin', 'admin', wait=False, headers={})
		if event == "search_freelance":
			self.listener = SearchFreelanceListener()
			self.conn.set_listener('',self.listener )
			self.conn.subscribe(destination="/topic/TOPIC_SEARCH_FREELANCE", ack='auto',id=1)

		if event == "freelance_apply_ao":
			self.listener = FreelanceApplyAoListener()
			self.conn.set_listener('',self.listener )
			self.conn.subscribe(destination="/topic/TOPIC_FREELANCE_APPLY_AO", ack='auto',id=1)


	def disconnect(self):
		self.conn.disconnect()
