# -*- coding: utf-8 -*-

from httplib2 import Http
from simplejson import dumps, loads
from datetime import datetime
import sys
import neo4j


import logging
from logging.handlers import RotatingFileHandler
# création de l'objet logger2 qui va nous servir à écrire dans les logs
logger2 = logging.getLogger("queries_logger")
# on met le niveau du logger2 à DEBUG, comme ça il écrit tout
logger2.setLevel(logging.DEBUG)
# création d'un formateur qui va ajouter le temps, le niveau
# de chaque message quand on écrira un message dans le log
formatter2 = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
# création d'un handler qui va rediriger une écriture du log vers
# un fichier en mode 'append', avec 1 backup et une taille max de 1Mo
file_handler2 = RotatingFileHandler('queries.log', 'a', 2000000, 10)
# on lui met le niveau sur DEBUG, on lui dit qu'il doit utiliser le formateur
# créé précédement et on ajoute ce handler au logger2
file_handler2.setLevel(logging.DEBUG)
file_handler2.setFormatter(formatter2)
logger2.addHandler(file_handler2)
# création d'un second handler qui va rediriger chaque écriture de log
# sur la console
steam_handler2 = logging.StreamHandler()
steam_handler2.setLevel(logging.DEBUG)
logger2.addHandler(steam_handler2)

#URL="http://dev.freelance.com:7474/db/data/transaction/commit"
URL="http://dev.freelance.com:7475"
#URL="http://dev.freelance.com:7474"
LOGIN = "neo4j"
PWD = "ASapps2016"

URL_TAGGER = "http://dev.freelance.com:8580/tagit"
#URL_TAGGER = "http://localhost:8580/tagit"

URL_ES = "http://dev.freelance.com:9200/neo4j-index-n01-node/_search/?pretty=1"#base 7475
#URL_ES = "http://dev.freelance.com:9200/neo4j-index-node/_search/?pretty=1"#base 7474

N_SOCKS = 0
SOCKS_LOG = 'socks.log'



def log_socks(func):
    def log(*args, **kwargs):
        with open(SOCKS_LOG,'a') as file:
            print '*****************N_SOCKS= ',N_SOCKS,' *************************'
            file.write(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+' - N_SOCKS= '+str(N_SOCKS)+'\n')
            f = func(*args, **kwargs)
            print '*****************N_SOCKS= ',N_SOCKS,' *************************'
            file.write(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+' - N_SOCKS= '+str(N_SOCKS)+'\n')
        return f
    return log

#@log_socks
def post_dict(dictionary):
    global N_SOCKS
    """Pass the whole dictionary as a json body to the url"""
    logger2.info('cypher query: %s',str(dictionary))
    connection = neo4j.connect(URL,username=LOGIN,password=PWD)
    cursor = connection.cursor()
    try:
        N_SOCKS +=1
        data = cursor.execute(dictionary.get("query","")).fetchall()
        connection.commit()
        logger2.info('resp neo4j OK: %s',str(data))
        cursor.close()
        connection.close()
        N_SOCKS -=1
        return ("OK",data)
    except:
        error = sys.exc_info()
        connection.rollback()
        logger2.info('resp neo4j KO: %s -> %s'%(error[0],error[1]))
        cursor.close()
        connection.close()
        N_SOCKS -=1
        return ("KO","")

def get_tagger(dictionary):
    http = Http()
    resp, data = http.request(
                                uri=URL_TAGGER,
                                method="POST",
                                body=dumps(dictionary),
                                )
    status = "OK" if resp.get('status')=="200" else "KO"
    return status,data        


def post_es(search_mode,node_type,node_property,query_text):

    command = {"query":  query_text,"fuzziness": "AUTO","operator":  "and"} if search_mode == "match" else query_text

    es_dict =  {
                  "query": {
                    "bool" : {
                      "must" : {
                        search_mode : { node_property :  command }
                      },
                  "filter": {
                    "type" : {
                        "value" : node_type
                    
                    }
                  }
                    
                    
                    }
                  }
                }

    http = Http()
    resp, data = http.request(
                                uri=URL_ES,
                                method="POST",
                                body=dumps(es_dict),
                                )
    status = "OK" if resp.get('status')=="200" else "KO"
    return status,loads(data,encoding='utf8')  