# -*- coding: utf-8 -*-

def flatten_json(y):
    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name +  a   + '__')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '__')
                i += 1
        else:
            out[name[:-2]] = x

    flatten(y)
    return out


def inflate_json(y):
    out = {}
    for key,val in y.items():
        temp_obj = out
        skey = key.split('__')
        second = key
        for i in range (0,len(skey)-1):
            #Recuperer indices dans les cles
            try:
                first = int(skey[i])
            except ValueError as err:
                first = skey[i]
            try:
                second = int(skey[i+1])
            except ValueError as err:
                second = skey[i+1]
            
            if type(first) == int:
                #savoir si index existe: sinon ajouter items temporaires
                if first+1 > len(temp_obj):
                    for temp_item in range(len(temp_obj),first+1):
                        temp_obj.append('waiting')
                if temp_obj[first] == 'waiting':
                    if type(second) == int:
                        pos = []
                        temp_obj[first] = pos
                    else:
                        pos = {}
                        temp_obj[first] = pos
                else:
                    pos = temp_obj[first]
            elif not temp_obj.has_key(first):
                if type(second) == int:
                    pos = []
                    temp_obj[first] = pos
                else:
                    pos = {}
                    temp_obj[first] = pos
            else:
                pos = temp_obj[first]
            temp_obj = pos

        if type(second) == int:
            if second+1 > len(temp_obj):
                #savoir si index existe: sinon ajouter items temporaires
                for sublist in range(len(temp_obj),second+1):
                    temp_obj.append('waiting')
        try:
            val = int(val)
        except ValueError as err:
            pass
        temp_obj[second] = val
    return out